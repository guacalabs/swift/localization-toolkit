// swift-tools-version:5.1
import PackageDescription

let package = Package(
    name: "LocalizationToolkit",
    platforms: [
        .iOS(.v11),
        .tvOS(.v11),
        .macOS(.v10_14)
    ],
    products: [
        .library(
            name: "LocalizationToolkit",
            targets: ["LocalizationToolkit"]
        )
    ],
    dependencies: [],
    targets: [
        .target(
            name: "LocalizationToolkit",
            dependencies: [],
            path: "./Sources"
        )
    ],
    swiftLanguageVersions: [.v5]
)
