#import <Foundation/Foundation.h>

//! Project version number for LocalizationToolkit.
FOUNDATION_EXPORT double LocalizationToolkitVersionNumber;

//! Project version string for LocalizationToolkit.
FOUNDATION_EXPORT const unsigned char LocalizationToolkitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <LocalizationToolkit/PublicHeader.h>
