import Foundation

public protocol Localizable {
    var key: String { get }
    var localized: String { get }
    var bundle: Bundle { get }
    static var tableName: String { get }
}

public extension Localizable {
    func localizedString(arguments: CVarArg...) -> String {
        bundle.localizedString(forKey: key, arguments: arguments, tableName: Self.tableName)
    }

    static var tableName: String {
        String(describing: self)
    }
}

public extension Localizable where Self: RawRepresentable, Self.RawValue == String {
    var key: String {
        rawValue
    }

    var localized: String {
        return bundle.localizedString(forKey: key, tableName: Self.tableName)
    }
}
