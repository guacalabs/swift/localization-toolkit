import Foundation

extension Bundle {
    func localizedString(forKey key: String, tableName: String = "Localizable") -> String {
        localizedString(forKey: key, value: key, table: tableName)
    }

    func localizedString(forKey key: String, arguments: [CVarArg], tableName: String = "Localizable") -> String {
        return withVaList(arguments) {
            NSString(
                format: localizedString(forKey: key, tableName: tableName),
                locale: Locale.current,
                arguments: $0
            )
        } as String
    }
}
